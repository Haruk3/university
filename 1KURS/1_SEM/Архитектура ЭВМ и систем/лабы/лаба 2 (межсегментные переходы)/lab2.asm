s_s segment
	db 20 DUP(?)
s_s ends

d_s1 segment
	a db 01001100b
d_s1 ends

d_s2 segment
	adr dd m1
d_s2 ends

d_s3 segment
	b db 11101100b
d_s3 ends

c_s1 segment
	assume cs:c_s1, ds:d_s1
	m1:
	mov ax, d_s1
	mov ds, ax
	xor ax, ax
	xor bx, bx
	mov al, a
	ror al, 3 ; циклический сдвиг вправо
	mov bl, al ; пропущенная строчка в текстовом варианте
	rcl bl, 1 ; циклический сдвиг влево через перенос
	jmp far ptr m2 ; переход во 2-й сегмент кода
c_s1 ends

c_s2 segment
	assume cs:c_s2, ds:d_s3
	m2:
	mov ax, d_s3
	mov ds, ax
	xor ax, ax
	xor bx, bx
	mov al, b
	ror al, 5 ; циклический сдвиг вправо
	mov bh, al ; пропущенная строчка в текстовом варианте
	rcl bh, 1 ; циклический сдвиг влево через перенос
	jmp far ptr exit ; безусловный межсегментный переход на метку конца
c_s2 ends

c_s3 segment
	assume cs:c_s3, ds:d_s1
	begin:
	mov ax, d_s1
	mov ds, ax
	xor ax, ax
	mov al, a
	shl al, 1 ; умножение на 2 путём лог. сдвига
	assume ds:d_s3
	mov ax, d_s3
	mov ds, ax
	xor ax, ax
	mov al, b
	shr al, 2 ; деление на 4 путём лог. сдвига
	assume ds:d_s2
	mov ax, d_s2
	mov ds, ax
	jmp adr ; безусловный переход на метку из 2 сегмента данных
	exit:
	mov ah, 4ch
	int 21h
c_s3 ends
	end begin