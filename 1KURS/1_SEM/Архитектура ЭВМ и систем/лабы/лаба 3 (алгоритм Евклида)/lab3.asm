; Реализация алгоритма Евклида на TASM

d_s segment
	a db 19h
	b db 5h
	nod db 00000001b
d_s ends

c_s segment
	assume cs:c_s, ds:d_s
	begin:
	mov ax, d_s
	mov ds, ax
	mov ah, a
	mov al, b
	mov bl, nod
	
	cycle:
	cmp ah, al
	jne m1 ; если a != b, переходим к метке m1
	mov bl, ah ; иначе сохраняем полученный НОД
	xor ax, ax ; чистим регистр AX
	mov cx, 8 ; число итераций перебора всех битов 1 байта
	jmp count ; и переходим к подсчёту единиц НОД
	
	m1:
	ja m2 ; если a > b, переходим к метке m2
	sub al, ah ; b = b - a
	jmp cycle ; переход к началу цикла

	m2:
	sub ah, al ; a = a - b
	jmp cycle ; переход к началу цикла
	
	count: ; переборный цикл
	ror bl, 1 ; цикл. сдвиг справо
	jc increment ; если CF=1, переходим к метке increment
	jmp m ; иначе loop
	
	increment:
	inc dl ; значение DL += 1
	
	m: loop count

	mov ah, 4ch
	int 21h
c_s ends
	end begin