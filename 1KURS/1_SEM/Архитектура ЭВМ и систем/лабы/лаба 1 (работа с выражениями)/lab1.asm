d_s segment
	a db 3
	b db 25
	c db 2
d_s ends
c_s segment
	assume ds:d_s, cs:c_s
	begin:
	mov ax, d_s
	mov ds, ax
	; Операция (a+c)
	mov ah, a
	mov al, c
	add al, ah
	xor ah, ah
	; Операция (a+c)^2
	mov bl, al
	mul bl
	xor bl, bl
	mov cl, al
	xor al, al
	; Операция (b-10c)
	mov al, 10
	mov bl, c
	mul bl
	mov bl, al
	xor al, al
	mov al, b
	sub al, bl
	; Операция (a+c)^2:(b-10c)
	mov bl, al
	xor al, al
	mov al, cl
	div bl
	; Очистка значений BX, CX
	xor bx, bx
	xor cx, cx
	; Логика
	xor ax, ax
	or ax, 10010100b
	not ax
	mov ah, 4ch
	int 21h
c_s ends
	end begin