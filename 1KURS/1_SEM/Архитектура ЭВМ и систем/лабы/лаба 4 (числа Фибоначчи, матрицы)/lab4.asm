d_s segment
	arr dw 18 dup(?)
	min dw 0
	max dw 0
d_s ends

c_s segment
	assume cs: c_s, ds: d_s
	begin:
	mov ax, d_s
	mov ds, ax
	
	; инициализация первых двух элементов массива
	mov si, 0
	mov arr[si], 0
	mov si, 2
	mov arr[si], 1
	
	mov si, 4 ; переход к 3-му элементу
	mov cx, 16 ; осталось вычислить еще 16 элементов
	
	fib: ; вычисление элементов ряда чисел Фибоначчи
		mov bx, arr[si-4]
		add bx, arr[si-2]
		mov arr[si], bx
		add si, 2
	loop fib ; переход на следующую итерацию
	
	; Модель рассматриваемой матрицы
	; [0,   1,   1,   2,   3,   5,  ]
	; [8,   13,  21,  34,  55,  89, ]
	; [144, 233, 377, 610, 987, 1597]
	
	mov si, 12 ; переход к 1-му элементу 2-й строки
	mov cx, 6 ; кол-во элементов в строке
	mov min, 0FFFFh ; макс. число для поиска минимума
	find_min: ; поиск минимума 2 строки
		mov ax, arr[si]
		rcr ax, 1
		jc even_false ; переход в случае нечётности (CF = 1)
		jmp next_row_item
	even_false:
		rcl ax, 1
		cmp ax, min
		jb save_min ; переход в случае ax < min (ax = arr[si])
		jmp next_row_item ; иначе переход к след. элементу
	save_min:
		mov min, ax
	next_row_item:
		add si, 2
	loop find_min ; переход на следующую итерацию
	
	mov si, 6 ; переход к 1-му элементу 4-го столбца
	mov cx, 3 ; кол-во элементов в столбце
	find_max: ; поиск максимума 4-го столбца
		mov ax, arr[si]
		rcr ax, 1
		jnc even_true ; переход в случае чётности (CF = 0)
		jmp next_col_item ; иначе переход к след. элементу
	even_true:
		rcl ax, 1
		cmp ax, max
		jg save_max
		jmp next_col_item
	save_max:
		mov max, ax
	next_col_item:
		add si, 12
	loop find_max ; переход на следующую итерацию
	
	; сохранение найденных min и max в регистрах BX и DX
	xor bx, bx
	mov bx, min
	xor dx, dx
	mov dx, max
	
	mov ah, 4ch
	int 21h
c_s ends
	end begin