import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class PlayerMovementController implements KeyListener {
    private boolean isMovingUp = false;
    private boolean isMovingDown = false;
    private boolean isMovingLeft = false;
    private boolean isMovingRight = false;

    private final int backgroundX;
    private final int backgroundY;
    private final Player player;

    public PlayerMovementController(Player player, int width, int height) {
        backgroundX = (width - 700)/2; // 290
        backgroundY = (height - 700)/2; // 15
        this.player = player;

    }

    public void update() {
        double speed = player.getSpeed();
        if (isMovingUp && player.getY() > (backgroundY + 5)) {
            player.addY(-speed);
        }
        if (isMovingDown && player.getY() < (backgroundY + 645)) {
            player.addY(speed);
        }
        if (isMovingLeft && player.getX() > (backgroundX+5)) {
            player.addX(-speed);
        }
        if (isMovingRight && player.getX() < (backgroundX + 650)) {
            player.addX(speed);
        }
        player.setCenterX(player.getX() + (double) player.getImage().getWidth() /2);
        player.setCenterY(player.getY() + (double) player.getImage().getHeight() /2);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if (keyCode == KeyEvent.VK_W) {
            isMovingUp = true;
        }
        if (keyCode == KeyEvent.VK_S) {
            isMovingDown = true;
        }
        if (keyCode == KeyEvent.VK_A) {
            isMovingLeft = true;
        }
        if (keyCode == KeyEvent.VK_D) {
            isMovingRight = true;
        }
        }


    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if (keyCode == KeyEvent.VK_W) {
            isMovingUp = false;
        }
        if (keyCode == KeyEvent.VK_S) {
            isMovingDown = false;
        }
        if (keyCode == KeyEvent.VK_A) {
            isMovingLeft = false;
        }
        if (keyCode == KeyEvent.VK_D) {
            isMovingRight = false;
        }
    }
}
