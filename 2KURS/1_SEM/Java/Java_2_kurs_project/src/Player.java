import java.awt.*;
public class Player extends Entity{
    private static final String IMAGE_NAME = "hero";

    private boolean isTimeStopped = false;
    private int count = 0;
    private int imgCount = 0;
    private int num = 1;

    public boolean isTimeStopped() {
        return isTimeStopped;
    }

    public void setIsTimeStopped(boolean isTimeStopped) {
        this.isTimeStopped = isTimeStopped;
    }



    public Player(int x, int y, int size) {
        super(x, y, size);
        setImageName(IMAGE_NAME + "0.png");
        setSpeed(3);

    }

    @Override
    public void update() {
        if (isTimeStopped){
            if (count++ >= 300){
                count = 0;
                isTimeStopped = false;
            }
        }
        if (imgCount++ >= 20){
            imgCount = 0;
            setImageName(IMAGE_NAME + (num++) + ".png");
            if (num > 1) num = 0;
        }

    }


    @Override
    public void draw(Graphics g) {
            g.drawImage(getImage(), (int) getX(),(int) getY(), 40, 40,  null);
    }
}
