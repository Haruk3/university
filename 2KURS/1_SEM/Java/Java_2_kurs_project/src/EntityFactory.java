import java.util.List;
import java.util.Random;
public class EntityFactory {
    private static final long ENEMY_SPAWN_DELAY = 10000;
    private static final long ENEMY_2_SPAWN_DELAY = 20000;
    private long trapSpawnDelay = 12000;
    private long hellStartDelay = 54000;
    private long buffSpawnDelay = 60000;

    private final Player player;
//    Тайминги начало
    private long lastTimeEnemySpawned = System.currentTimeMillis();
    private long lastTimeEnemy2Spawned = System.currentTimeMillis();
    private long lastTimeTrapSpawned = System.currentTimeMillis();
    private long lastTimeHellStarted = System.currentTimeMillis();
    private long lastTimeBuffSpawned = System.currentTimeMillis();
//    Тайминги конец

    private final int backgroundX;
    private final int backgroundY;

    public EntityFactory(Player player, int width, int height) {
        backgroundX = (width - 700)/2; // 290
        backgroundY = (height - 700)/2; // 15
        this.player = player;
    }

    public List<Entity> getEntities() {
        //? Enemy Spawn
        if ((System.currentTimeMillis() - lastTimeEnemySpawned) > ((Math.random() * ENEMY_SPAWN_DELAY) + (ENEMY_SPAWN_DELAY / 2.0))) {
            lastTimeEnemySpawned = System.currentTimeMillis();
            return List.of(new Enemy((int) (player.getX() + (Math.random() * 800 * getRandomOneOrMinusOne() + 700 * getRandomOneOrMinusOne())),
                    (int) (player.getY() - Math.random() * 600 * getRandomOneOrMinusOne() - 700 * getRandomOneOrMinusOne()),
                    (int) (Math.random() * 15 + 30), player));
        }

        if ((System.currentTimeMillis() - lastTimeEnemy2Spawned) > ((Math.random() * ENEMY_2_SPAWN_DELAY) + (ENEMY_2_SPAWN_DELAY / 2.0))) {
            lastTimeEnemy2Spawned = System.currentTimeMillis();
            return List.of(new Enemy((int) (player.getX() + (Math.random() * 800 * getRandomOneOrMinusOne() + 700 * getRandomOneOrMinusOne())),
                    (int) (player.getY() - Math.random() * 600 * getRandomOneOrMinusOne() - 700 * getRandomOneOrMinusOne()),
                    (int) (Math.random() * 15 + 30), player));
        }

        //? Trap Spawn
        if ((System.currentTimeMillis() - lastTimeTrapSpawned) > ((Math.random() * trapSpawnDelay) + (trapSpawnDelay / 2.0))) {
            lastTimeTrapSpawned = System.currentTimeMillis();
            return List.of(new Trap((int) (Math.random() * 600 + backgroundX + 50), (int) (Math.random() * 550 + backgroundY + 50)));
        }

        //? Buff Spawn
        if ((System.currentTimeMillis() - lastTimeBuffSpawned) > ((Math.random() * buffSpawnDelay) + (buffSpawnDelay / 2.0))) {
            lastTimeBuffSpawned = System.currentTimeMillis();
            return List.of(new PlayerBuff((int) (Math.random() * 600 + backgroundX + 50), (int) (Math.random() * 550 + backgroundY + 50)));
        }

        //? Hell Start
        if ((System.currentTimeMillis() - lastTimeHellStarted) > ((Math.random() * hellStartDelay) + (hellStartDelay / 2.0))) {
            lastTimeHellStarted = System.currentTimeMillis();
            if (trapSpawnDelay > 5000) trapSpawnDelay -= 2000;
            if (hellStartDelay > 15000) hellStartDelay -= 4000;
            if(buffSpawnDelay > 23000) buffSpawnDelay -= 3000;
            if (player.getSpeed() < 7) player.setSpeed(player.getSpeed() + 2);
            return List.of(
            (new Enemy((int) (player.getX() + (Math.random() * 800 * getRandomOneOrMinusOne() + 700 * getRandomOneOrMinusOne())),
                    (int) (player.getY() - Math.random() * 600 * getRandomOneOrMinusOne() - 700 * getRandomOneOrMinusOne()),
                    (int) (Math.random() * 15 + 30), player)),
            (new Enemy((int) (player.getX() + (Math.random() * 800 * getRandomOneOrMinusOne() + 700 * getRandomOneOrMinusOne())),
                    (int) (player.getY() - Math.random() * 600 * getRandomOneOrMinusOne() - 700 * getRandomOneOrMinusOne()),
                    (int) (Math.random() * 15 + 30), player)),
            (new Enemy((int) (player.getX() + (Math.random() * 800 * getRandomOneOrMinusOne() + 700 * getRandomOneOrMinusOne())),
                    (int) (player.getY() - Math.random() * 600 * getRandomOneOrMinusOne() - 700 * getRandomOneOrMinusOne()),
                    (int) (Math.random() * 15 + 30), player)),
            (new Enemy((int) (player.getX() + (Math.random() * 800 * getRandomOneOrMinusOne() + 700 * getRandomOneOrMinusOne())),
                    (int) (player.getY() - Math.random() * 600 * getRandomOneOrMinusOne() - 700 * getRandomOneOrMinusOne()),
                    (int) (Math.random() * 15 + 30), player)),
            (new Enemy((int) (player.getX() + (Math.random() * 800 * getRandomOneOrMinusOne() + 700 * getRandomOneOrMinusOne())),
                    (int) (player.getY() - Math.random() * 600 * getRandomOneOrMinusOne() - 700 * getRandomOneOrMinusOne()),
                    (int) (Math.random() * 15 + 30), player))
            );
        }

        return null;
    }

    private static int getRandomOneOrMinusOne() {
        Random random = new Random();
        return random.nextBoolean() ? 1 : -1;
    }
}
