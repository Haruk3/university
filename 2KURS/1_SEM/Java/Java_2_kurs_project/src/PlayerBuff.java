import java.awt.*;

public class PlayerBuff extends Entity{
    private final TypeOfPlayerBuff typeOfPlayerBuff;
    private int count = 0;
    private boolean stillThere = true;

    public PlayerBuff(int x, int y) {
        super(x, y);
        setSize(40);
        typeOfPlayerBuff = TypeOfPlayerBuff.getRandomValue();
        switch (typeOfPlayerBuff){
            case KILL_ALL_ENEMIES -> setImageName("dynamite.png");
            case TIME_STOP -> setImageName("clock.png");
        }
    }

    public TypeOfPlayerBuff getTypeOfPlayerBuff() {
        return typeOfPlayerBuff;
    }

    public boolean isStillThere() {
        return stillThere;
    }

    @Override
    public void update() {
        if (stillThere && count++ >= 300) stillThere = false;
    }

    @Override
    public void draw(Graphics g) {
        g.drawImage(getImage(), (int) getX(),(int) getY(), null);

    }
}
