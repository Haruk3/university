#include <windows.h>
#include <stdio.h>

BOOL RegClass(WNDPROC, LPCTSTR, UINT);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HINSTANCE hInst;
char szClassName[] = "WindowsAppClass";
HWND myHwnd = NULL;
int badDaddy = 55;

void ShowError(const char *errorText)
{
    DWORD errorCode = GetLastError();
    LPVOID errorMsg;
    char message[256];

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        errorCode,
        0,
        (LPSTR)&errorMsg,
        0,
        NULL);

    sprintf(message, "Error code: %d\n%s\n%s", errorCode, errorText, (LPSTR)errorMsg);
    MessageBox(NULL, message, "ERROR", MB_ICONERROR);
    LocalFree(errorMsg);
}

BOOL RegClass(WNDPROC Proc, LPCTSTR szName, UINT brBackground)
{
    WNDCLASS wc;
    wc.style = wc.cbClsExtra = wc.cbWndExtra = 0;
    wc.lpfnWndProc = Proc;
    wc.hInstance = hInst;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(brBackground + 1);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = szName;

    return (RegisterClass(&wc) != 0);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    MSG msg;
    HWND hwnd;
    hInst = hInstance;

    if (!RegClass(WndProc, szClassName, COLOR_WINDOW))
    {
        return FALSE;
    }

    hwnd = CreateWindow(
        szClassName,
        "Parent Application",
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT,
        500, 500,
        0,
        0,
        hInstance,
        NULL);
    if (!hwnd)
    {
        return FALSE;
    }

    while (GetMessage(&msg, 0, 0, 0))
    {
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
    case WM_LBUTTONDOWN:
    {
        if (!RegClass(WndProc, szClassName, COLOR_WINDOW))
        {
            ShowError("ERROR REGISTRATING WINDOW CLASS");
            return FALSE;
        }
        return 0;
    }
    case WM_RBUTTONDOWN:
    {

        myHwnd = CreateWindow(
            "static",
            "Test AppWindow",
            WS_OVERLAPPEDWINDOW | WS_VISIBLE,
            CW_USEDEFAULT, CW_USEDEFAULT,
            100, 100,
            badDaddy,
            0,
            hInst,
            NULL);

        if (!myHwnd)
        {
            ShowError("ERROR CREATING APP`s WINDOW");
        }
        return 0;
    }

    case WM_MBUTTONDOWN:
    {
        DeleteFile("\\user1\\test\\test1.txt");
        ShowError("ERROR. CAN`T DELETE FILE");
        break;
    }
    case WM_DESTROY:
    {
        PostQuitMessage(0);
        return 0;
    }
    default:
        return DefWindowProc(hwnd, msg, wParam, lParam);
    }
}
