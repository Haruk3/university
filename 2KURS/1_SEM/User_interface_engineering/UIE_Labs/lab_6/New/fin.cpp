#include <windows.h>
#include <stdio.h>
#include <vector>
#include <limits>
#include <string>

LRESULT CALLBACK DrawWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ListWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

using namespace std;

bool drawing = false;
int lastY = std::numeric_limits<int>::max(), lastX = std::numeric_limits<int>::max(), lastFigureIndex = 0, selectedIndex = -1, numberOfPoints = 0;
COLORREF lineColor = RGB(0, 0, 0), fillColor = RGB(255, 0, 0);
HWND hwndListBox, drawWindow, listWindow;
HINSTANCE hInst;

class Figure
{
private:
    std::vector<POINT> points;
    bool collored = false;

public:
    bool wasCollored = false;
    Figure(){};
    Figure(Figure *figure)
    {
        collored = figure->isCollored();
        for (POINT point : figure->getPoints())
        {
            points.push_back({point.x, point.y});
        }
    };
    POINT getFirstPoint()
    {
        return points[0];
    }
    void setCollored()
    {
        collored = !collored;
        wasCollored = !wasCollored;
    }
    bool isCollored()
    {
        return collored;
    }

    POINT getLastPoint()
    {
        return points.back();
    }
    std::vector<POINT> getPoints()
    {
        return points;
    }
    void addPoint(POINT point)
    {
        points.push_back(point);
    }
    bool IsPointInsidePolygon(const POINT &point)
    {
        int crossings = 0;
        size_t numVertices = points.size();

        for (size_t i = 0; i < numVertices; i++)
        {
            const POINT &vertex1 = points[i];
            const POINT &vertex2 = points[(i + 1) % numVertices];

            if ((vertex1.y <= point.y && vertex2.y > point.y) ||
                (vertex1.y > point.y && vertex2.y <= point.y))
            {
                if (point.x < (vertex2.x - vertex1.x) * (point.y - vertex1.y) / (vertex2.y - vertex1.y) + vertex1.x)
                {
                    crossings++;
                }
            }
        }
        return (crossings % 2 != 0);
    }

    void draw(HWND hwnd, COLORREF fillColor2 = fillColor)
    {
        HDC hdc = GetDC(hwnd);
        HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
        HBRUSH hBrush = CreateSolidBrush(fillColor2);
        for (int i = 0; i < points.size() - 1; i++)
        {
            SelectObject(hdc, hPen);
            SelectObject(hdc, hBrush);
            MoveToEx(hdc, points[i].x, points[i].y, NULL);
            LineTo(hdc, points[i + 1].x, points[i + 1].y);
        }
        if (collored && points[0].x == points.back().x && points[0].y == points.back().y)
        {
            Polygon(hdc, points.data(), points.size() - 1);
        }
        DeleteObject(hPen);
        DeleteObject(hBrush);
        ReleaseDC(hwnd, hdc);
    }
};

static std::vector<Figure *> figures;

void mouseMove(HWND hwnd,WPARAM wParam, LPARAM lParam);
void createPoint(HWND hwnd,WPARAM wParam, LPARAM lParam);
void createFigure(HWND hwnd,WPARAM wParam, LPARAM lParam);
void fillPolygon(HWND hwnd,WPARAM wParam, LPARAM lParam);
void openArchive(HWND hwnd,WPARAM wParam, LPARAM lParam);
void closeArchive(HWND hwnd,WPARAM wParam, LPARAM lParam);
void moveUp(HWND hwnd,WPARAM wParam, LPARAM lParam);
void moveDown(HWND hwnd,WPARAM wParam, LPARAM lParam);
void loadFigure(HWND hwnd,WPARAM wParam, LPARAM lParam);
void finish(HWND hwnd,WPARAM wParam, LPARAM lParam);

enum State
{
    NONE,
    MAIN_WINDOW,
    SECOND_POINT_WINDOW,
    POINT_WINDOW,
    DRAWED_FIGURES_WINDOW,
    ARCHIVE,
    FINISH_SUB,
    FINISH_MAIN
};

State transition[7][8] = {
    {FINISH_MAIN, SECOND_POINT_WINDOW, NONE,                  NONE,    NONE,    NONE,    NONE,                  NONE}, // main window 
    {FINISH_MAIN, POINT_WINDOW,        NONE,                  NONE,    NONE,    NONE,    NONE,                  NONE}, // second point window
    {FINISH_MAIN, POINT_WINDOW,        DRAWED_FIGURES_WINDOW, NONE,    NONE,    NONE,    NONE,                  NONE}, // point window
    {FINISH_MAIN, SECOND_POINT_WINDOW, DRAWED_FIGURES_WINDOW, ARCHIVE, NONE,    NONE,    NONE,                  DRAWED_FIGURES_WINDOW}, // draw window
    {NONE,        NONE,                NONE,                  NONE,    ARCHIVE, ARCHIVE, DRAWED_FIGURES_WINDOW, NONE}, // archive
    {NONE,        NONE,                NONE,                  NONE,    NONE,    NONE,    NONE,                  NONE},
    {NONE,        NONE,                NONE,                  NONE,    NONE,    NONE,    NONE,                  NONE},
};

void (*action[7][8])(HWND hwnd,WPARAM wParam, LPARAM lParam) = {
    {finish, createPoint, NULL,         NULL,        NULL,     NULL,   NULL,         NULL},
    {finish, createPoint, NULL,         NULL,        NULL,     NULL,   NULL,         NULL},
    {finish, createPoint, createFigure, NULL,        NULL,     NULL,   NULL,         NULL},
    {finish, createPoint, fillPolygon,  openArchive, NULL,     NULL,   NULL,         loadFigure},
    {NULL,   NULL,        NULL,         NULL,        moveDown, moveUp, closeArchive, NULL},
    {NULL,   NULL,        NULL,         NULL,        NULL,     NULL,   NULL,         NULL},
    {NULL,   NULL,        NULL,         NULL,        NULL,     NULL,   NULL,         NULL}
};

State currentState = MAIN_WINDOW;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASS drawWindowClass = {0};
    drawWindowClass.lpfnWndProc = DrawWindow;
    drawWindowClass.hInstance = hInstance;
    drawWindowClass.lpszClassName = "drawWindowClass";
    RegisterClass(&drawWindowClass);
    drawWindow = CreateWindow("drawWindowClass", "Draw window", WS_OVERLAPPEDWINDOW,
                              100, 100, 800, 720, nullptr, nullptr, hInstance, nullptr);
    ShowWindow(drawWindow, nCmdShow);
    MSG msg = {0};
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT CALLBACK DrawWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
    case WM_CREATE:{
        WNDCLASS listWindowClass = {0};

        listWindowClass.lpfnWndProc = ListWindow;
        listWindowClass.hInstance = hInst;
        listWindowClass.lpszClassName = "listWindowClass";
        RegisterClass(&listWindowClass);

        listWindow = CreateWindow(
            "listWindowClass",
            "Second Window",
            WS_OVERLAPPEDWINDOW & ~WS_SYSMENU,
            900, 100,
            400, 600,
            hwnd, nullptr, hInst, nullptr);
        hwndListBox = CreateWindow(
            "LISTBOX",
            "list",
            WS_CHILD | WS_VISIBLE | WS_VSCROLL | LBS_NOTIFY | LBS_WANTKEYBOARDINPUT,
            10, 10,
            100, 300,
            listWindow,
            NULL,
            hInst,
            NULL);

        figures.push_back(new Figure());
        return 0;
    }
    default:{
        switch (currentState)
        {
        case NONE: return 0;

        case MAIN_WINDOW:{
            if (message == WM_DESTROY){
                currentState = transition[0][0];
                action[0][0](hwnd, wParam, lParam);
                return 0;
            }
            if (message == WM_LBUTTONDOWN){
                currentState = transition[0][1];
                action[0][1](hwnd, wParam, lParam);
                return 0;
            }
            return DefWindowProc(hwnd, message, wParam, lParam);
        }

        case SECOND_POINT_WINDOW:{
            if (message == WM_DESTROY){
                currentState = transition[1][0];
                action[1][0](hwnd, wParam, lParam);
                return 0;
            }
            if (message == WM_LBUTTONDOWN){
                currentState = transition[1][1];
                action[1][1](hwnd, wParam, lParam);
                return 0;
            }
            if (message == WM_MOUSEMOVE){
                mouseMove(hwnd, wParam, lParam);
                return 0;
            }
            
            return DefWindowProc(hwnd, message, wParam, lParam);
        }

        case POINT_WINDOW:{
            if (message == WM_DESTROY){
                currentState = transition[2][0];
                action[2][0](hwnd, wParam, lParam);
                return 0;
            }
            if (message == WM_LBUTTONDOWN){
                currentState = transition[2][1];
                action[2][1](hwnd, wParam, lParam);
                return 0;
            }
            if (message == WM_RBUTTONDOWN){
                currentState = transition[2][2];
                action[2][2](hwnd, wParam, lParam);
                return 0;
            }
            if (message == WM_MOUSEMOVE){
                mouseMove(hwnd, wParam, lParam);
                return 0;
            }
            return DefWindowProc(hwnd, message, wParam, lParam);
        }

        case DRAWED_FIGURES_WINDOW:{
            if (message == WM_DESTROY){
                currentState = transition[3][0];
                action[3][0](hwnd, wParam, lParam);
                return 0;
            }
            if (message == WM_LBUTTONDOWN){
                currentState = transition[3][1];
                action[3][1](hwnd, wParam, lParam);
                return 0;
            }
            if (message == WM_RBUTTONDOWN){
                currentState = transition[3][2];
                action[3][2](hwnd, wParam, lParam);
                return 0;
            }
            if (message == WM_MBUTTONDOWN){
                currentState = transition[3][3];
                action[3][3](hwnd, wParam, lParam);
                return 0;
            }
            if (message == WM_APP){
                currentState = transition[3][7];
                action[3][7](hwnd, wParam, lParam);
                return 0;
            }
            return DefWindowProc(hwnd, message, wParam, lParam);
        }

        default: return DefWindowProc(hwnd, message, wParam, lParam);
        }
    }
    }
}

LRESULT CALLBACK ListWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (currentState == ARCHIVE){
        if (message == WM_KEYDOWN && wParam == VK_DOWN){
            currentState = transition[4][4];
            action[4][4](hwnd, wParam, lParam);
            return 0;
        }
        if (message == WM_KEYDOWN && wParam == VK_UP){
            currentState = transition[4][5];
            action[4][5](hwnd, wParam, lParam);
            return 0;
        }
        if (message == WM_KEYDOWN && wParam == VK_RETURN){
            currentState = transition[4][6];
            action[4][6](hwnd, wParam, lParam);
            return 0;
        }
        if (message == WM_DESTROY){
            PostQuitMessage(0);
            return 0;
        }
    }
    return DefWindowProc(hwnd, message, wParam, lParam);
}


void mouseMove(HWND hwnd, WPARAM wParam, LPARAM lParam){
    if (drawing){
        Sleep(40);
        HDC hdc = GetDC(hwnd);
        if (lastX != std::numeric_limits<int>::max() && lastY != std::numeric_limits<int>::max())
        {
            MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
            SetROP2(hdc, R2_NOTXORPEN);
            HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
            SelectObject(hdc, hPen);
            LineTo(hdc, lastX, lastY);
        }

        MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
        LineTo(hdc, LOWORD(lParam), HIWORD(lParam));
        lastX = LOWORD(lParam), lastY = HIWORD(lParam);
        ReleaseDC(hwnd, hdc);
    }
}

void createPoint(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
    figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
    numberOfPoints++;
    drawing = true;
}
void createFigure(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
    numberOfPoints = 0;
    Figure *figure = figures.back();
    drawing = false;
    figure->addPoint({LOWORD(lParam), HIWORD(lParam)});
    figure->addPoint(figure->getFirstPoint());
    figures.push_back(new Figure());
    lastX = lastY = std::numeric_limits<int>::max();
    figure->draw(hwnd);
    std::string numOfFigure = "Figure " + std::to_string(lastFigureIndex++);
    SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)numOfFigure.c_str());
}
void fillPolygon(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
    POINT clickPoint = {LOWORD(lParam), HIWORD(lParam)};
    std::vector<Figure *> tmp = figures;
    int i = 0;
    for (Figure *figure : tmp)
    {
        if (figure == tmp.back() || figure->isCollored() || figure->wasCollored) continue;
        if (figure->IsPointInsidePolygon(clickPoint)){
            Figure *emptyFigure = figures.back();
            figure->wasCollored = true;
            figures.back() = new Figure(figure);
            figures.back()->setCollored();
            figures.back()->draw(hwnd);
            std::string numOfFigure = "Figure " + std::to_string(lastFigureIndex++);
            SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)numOfFigure.c_str());
            figures.push_back(emptyFigure);
        }
    }
}
void openArchive(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
    ShowWindow(listWindow, SW_SHOW);
    SetFocus(listWindow);
}
void closeArchive(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
    if (selectedIndex != -1)
    {
        int index = SendMessage(hwndListBox, LB_GETCURSEL, 0, 0);
        SendMessage(drawWindow, WM_APP, index, 0);
        SetFocus(drawWindow);
        ShowWindow(hwnd, SW_HIDE);
    }
}
void moveUp(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
    if ((--selectedIndex) < 0)
        selectedIndex = 0;
    SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
}
void moveDown(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
    int num = SendMessage(hwndListBox, LB_GETCOUNT, 0, 0) - 1;
    if (num < ++selectedIndex)
        selectedIndex = num;
    SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
}
void loadFigure(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
    figures[wParam]->draw(hwnd, RGB(rand() % 255, rand() % 255, rand() % 255));
}
void finish(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
    PostQuitMessage(0);
}