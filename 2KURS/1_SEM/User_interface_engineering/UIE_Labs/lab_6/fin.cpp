#include <windows.h>
#include <stdio.h>
#include <vector>
#include <limits>
#include <string>

LRESULT CALLBACK DrawWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ListWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

HINSTANCE hInst;
int lastY = std::numeric_limits<int>::max();
int lastX = std::numeric_limits<int>::max();
COLORREF lineColor = RGB(0, 0, 0);
COLORREF fillColor = RGB(255, 0, 0);
int lastFigureIndex = 0;
int selectedIndex = -1;
HWND hwndListBox, drawWindow;
bool drawing = false;
HWND listWindow;
int numberOfPoints = 0;

class Figure
{
private:
    std::vector<POINT> points;
    bool collored = false;

public:
    bool wasCollored = false;
    Figure(){};
    Figure(Figure *figure)
    {
        collored = figure->isCollored();
        for (POINT point : figure->getPoints())
        {
            points.push_back({point.x, point.y});
        }
    };
    POINT getFirstPoint()
    {
        return points[0];
    }
    void setCollored()
    {
        collored = !collored;
        wasCollored = !wasCollored;
    }
    bool isCollored()
    {
        return collored;
    }

    POINT getLastPoint()
    {
        return points.back();
    }
    std::vector<POINT> getPoints()
    {
        return points;
    }
    void addPoint(POINT point)
    {
        points.push_back(point);
    }
    bool IsPointInsidePolygon(const POINT &point)
    {
        int crossings = 0;
        size_t numVertices = points.size();

        for (size_t i = 0; i < numVertices; i++)
        {
            const POINT &vertex1 = points[i];
            const POINT &vertex2 = points[(i + 1) % numVertices];

            if ((vertex1.y <= point.y && vertex2.y > point.y) ||
                (vertex1.y > point.y && vertex2.y <= point.y))
            {
                if (point.x < (vertex2.x - vertex1.x) * (point.y - vertex1.y) / (vertex2.y - vertex1.y) + vertex1.x)
                {
                    crossings++;
                }
            }
        }
        return (crossings % 2 != 0);
    }

    void draw(HWND hwnd, COLORREF fillColor2 = fillColor)
    {
        HDC hdc = GetDC(hwnd);
        HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
        HBRUSH hBrush = CreateSolidBrush(fillColor2);
        for (int i = 0; i < points.size() - 1; i++)
        {
            SelectObject(hdc, hPen);
            SelectObject(hdc, hBrush);
            MoveToEx(hdc, points[i].x, points[i].y, NULL);
            LineTo(hdc, points[i + 1].x, points[i + 1].y);
        }
        if (collored && points[0].x == points.back().x && points[0].y == points.back().y)
        {
            Polygon(hdc, points.data(), points.size() - 1);
        }
        DeleteObject(hPen);
        DeleteObject(hBrush);
        ReleaseDC(hwnd, hdc);
    }
};
static std::vector<Figure *> figures;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASS drawWindowClass = {0};
    drawWindowClass.lpfnWndProc = DrawWindow;
    drawWindowClass.hInstance = hInstance;
    drawWindowClass.lpszClassName = "drawWindowClass";
    RegisterClass(&drawWindowClass);
    drawWindow = CreateWindow("drawWindowClass", "Draw window", WS_OVERLAPPEDWINDOW,
                              100, 100, 800, 720, nullptr, nullptr, hInstance, nullptr);
    ShowWindow(drawWindow, nCmdShow);
    MSG msg = {0};
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT CALLBACK DrawWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
    case WM_CREATE:
    {
        WNDCLASS listWindowClass = {0};

        listWindowClass.lpfnWndProc = ListWindow;
        listWindowClass.hInstance = hInst;
        listWindowClass.lpszClassName = "listWindowClass";
        RegisterClass(&listWindowClass);

        listWindow = CreateWindow(
            "listWindowClass",
            "Second Window",
            WS_OVERLAPPEDWINDOW & ~WS_SYSMENU,
            900, 100,
            400, 600,
            nullptr, nullptr, hInst, nullptr);
        hwndListBox = CreateWindow(
            "LISTBOX",
            "list",
            WS_CHILD | WS_VISIBLE | WS_VSCROLL | LBS_NOTIFY | LBS_WANTKEYBOARDINPUT,
            10, 10,
            100, 300,
            listWindow,
            NULL,
            hInst,
            NULL);

        figures.push_back(new Figure());
        return 0;
    }

    case WM_SETFOCUS:
    {
        ShowWindow(listWindow, SW_HIDE);
        return 0;
    }
    case WM_LBUTTONDOWN:
        figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
        numberOfPoints++;
        drawing = true;
        return 0;

    case WM_RBUTTONDOWN:
    {
        if (drawing && numberOfPoints > 1)
        {
            numberOfPoints = 0;
            Figure *figure = figures.back();
            drawing = false;
            figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
            figure->addPoint(figure->getFirstPoint());
            figures.push_back(new Figure());
            lastX = lastY = std::numeric_limits<int>::max();
            figure->draw(hwnd);
            std::string numOfFigure = "Figure " + std::to_string(lastFigureIndex++);
            SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)numOfFigure.c_str());
            return 0;
        }
        if (figures.size() < 2)
            return 0;
        POINT clickPoint = {LOWORD(lParam), HIWORD(lParam)};
        std::vector<Figure *> tmp = figures;
        int i = 0;
        for (Figure *figure : tmp)
        {
            if (figure == tmp.back() || figure->isCollored() || figure->wasCollored)
                continue;
            if (figure->IsPointInsidePolygon(clickPoint))
            {

                Figure *emptyFigure = figures.back();
                figure->wasCollored = true;
                figures.back() = new Figure(figure);
                figures.back()->setCollored();
                figures.back()->draw(hwnd);
                std::string numOfFigure = "Figure " + std::to_string(lastFigureIndex++);
                SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)numOfFigure.c_str());
                figures.push_back(emptyFigure);
            }
        }
        return 0;
    }
    case WM_MBUTTONDOWN:
    {
        if (figures.size() > 1)
        {
            ShowWindow(listWindow, SW_SHOW);
            SetFocus(listWindow);
        }
        return 0;
    }
    case WM_MOUSEMOVE:
    {
        if (drawing)
        {
            Sleep(40);
            HDC hdc = GetDC(hwnd);
            if (lastX != std::numeric_limits<int>::max() && lastY != std::numeric_limits<int>::max())
            {
                MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
                SetROP2(hdc, R2_NOTXORPEN);
                HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
                SelectObject(hdc, hPen);
                LineTo(hdc, lastX, lastY);
            }

            MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
            LineTo(hdc, LOWORD(lParam), HIWORD(lParam));
            lastX = LOWORD(lParam), lastY = HIWORD(lParam);
            ReleaseDC(hwnd, hdc);
        }
        return 0;
    }

    case WM_APP:
    {
        figures[wParam]->draw(hwnd, RGB(rand() % 255, rand() % 255, rand() % 255));
        return 0;
    }

    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

    default:
        return DefWindowProc(hwnd, message, wParam, lParam);
    }
}

LRESULT CALLBACK ListWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {

    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

    case WM_KEYDOWN:
    {
        if (drawing)
            return 0;
        switch (wParam)
        {
        case VK_UP:
        {
            if ((--selectedIndex) < 0)
                selectedIndex = 0;
            SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
            return 0;
        }

        case VK_DOWN:
        {
            int num = SendMessage(hwndListBox, LB_GETCOUNT, 0, 0) - 1;
            if (num < ++selectedIndex)
                selectedIndex = num;
            SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
            return 0;
        }
        case VK_RETURN:
        {

            if (selectedIndex != -1)
            {
                int index = SendMessage(hwndListBox, LB_GETCURSEL, 0, 0);
                SendMessage(drawWindow, WM_APP, index, 0);
                SetFocus(drawWindow);
                ShowWindow(hwnd, SW_HIDE);
            }
            return 0;
        }
        default:
            break;
        }
        break;
    }

    default:
        return DefWindowProc(hwnd, message, wParam, lParam);
    }
}
