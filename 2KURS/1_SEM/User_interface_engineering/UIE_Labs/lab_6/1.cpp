#include <windows.h>
#include <vector>
#include <limits>
#include <string>

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ListWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

using namespace std;

COLORREF lineColor = RGB(0, 0, 0);
COLORREF fillColor = RGB(255, 0, 0);
int lastY = numeric_limits<int>::max();
int lastX = numeric_limits<int>::max();
HINSTANCE hInst;
HWND hwndList, hwndListBox;
int lastFigureIndex = 0;
int selectedIndex = -1;
class Figure
{
    vector<POINT> points;

public:
    POINT getFirstPoint()
    {
        return points[0];
    }
    POINT getLastPoint()
    {
        return points.back();
    }
    void addPoint(POINT point)
    {
        points.push_back(point);
    }

    void draw(HWND hwnd)
    {
        HDC hdc = GetDC(hwnd);
        HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
        HBRUSH hBrush = CreateSolidBrush(fillColor);
        for (int i = 0; i < points.size() - 1; i++)
        {
            SelectObject(hdc, hPen);
            SelectObject(hdc, hBrush);
            MoveToEx(hdc, points[i].x, points[i].y, NULL);
            LineTo(hdc, points[i + 1].x, points[i + 1].y);
        }
        if (points[0].x == points.back().x && points[0].y == points.back().y)
        {
            Polygon(hdc, points.data(), points.size() - 1);
        }
        DeleteObject(hPen);
        DeleteObject(hBrush);
        ReleaseDC(hwnd, hdc);
    }
};
vector<Figure *> figures;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASS wc = {0};
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = "DrawingAppClass";
    hInst = hInstance;

    RegisterClass(&wc);

    HWND hwnd = CreateWindow(
        "DrawingAppClass",
        "Drawing Application",
        WS_OVERLAPPEDWINDOW,
        300, 200,
        800, 600,
        NULL, NULL, hInstance, NULL);

    ShowWindow(hwnd, nCmdShow);

    MSG msg = {0};
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static bool drawing = false;

    switch (uMsg)
    {
    case WM_CREATE:
    {
        WNDCLASS wcList = {0};
        wcList.lpfnWndProc = ListWindowProc;
        wcList.hInstance = hInst;
        wcList.lpszClassName = "FigureListClass";
        RegisterClass(&wcList);
        hwndList = CreateWindowEx(
            0,
            "FigureListClass",
            "Figure List",
            WS_OVERLAPPEDWINDOW | WS_VISIBLE,
            1120, 200,
            400, 600,
            hwnd, NULL, hInst, NULL);

        // ShowWindow(figuresList, SW_SHOW);

        figures.push_back(new Figure());

        return 0;
    }

    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

    case WM_LBUTTONDOWN:
        figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
        drawing = true;
        return 0;

    case WM_RBUTTONDOWN:
    {
        if (drawing)
        {
            Figure *figure = figures.back();
            drawing = false;
            figures.back()->addPoint({LOWORD(lParam), HIWORD(lParam)});
            figure->addPoint(figure->getFirstPoint());
            figures.push_back(new Figure());
            lastX = lastY = numeric_limits<int>::max();
            figure->draw(hwnd);
            string numOfFigure = "Figure " + to_string(lastFigureIndex++);
            // char str[] = lastFigureIndex++;

            // SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)str);
            // SendMessage(hwndList, WM_APP, 0, 0);
            SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)numOfFigure.c_str());
            // SendMessage(hwndListBox, CB_SETCURSEL, 0, 0);
            // InvalidateRect(hwndList, NULL, TRUE);
        }

        return 0;
    }

    case WM_MOUSEMOVE:
    {
        if (drawing)
        {
            Sleep(40);
            HDC hdc = GetDC(hwnd);
            if (lastX != numeric_limits<int>::max() && lastY != numeric_limits<int>::max())
            {
                MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
                SetROP2(hdc, R2_NOTXORPEN);
                HPEN hPen = CreatePen(PS_SOLID, 1, lineColor);
                SelectObject(hdc, hPen);
                LineTo(hdc, lastX, lastY);
            }

            MoveToEx(hdc, figures.back()->getLastPoint().x, figures.back()->getLastPoint().y, NULL);
            LineTo(hdc, LOWORD(lParam), HIWORD(lParam));
            lastX = LOWORD(lParam), lastY = HIWORD(lParam);
            ReleaseDC(hwnd, hdc);
        }
        return 0;
    }
    case WM_KEYDOWN:
    {
        switch (wParam)
        {
        case VK_UP:
        {
            // Обработка стрелки вверх
            if ((selectedIndex - 1) < 0)
                selectedIndex = 0;
            printf("%d\n", selectedIndex);
            SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
            InvalidateRect(hwnd, NULL, TRUE);
            return 0;
        }

        case VK_DOWN:
        {
            // Обработка стрелки вниз
            int num = SendMessage(hwndListBox, LB_GETCOUNT, 0, 0) - 1;
            if (num < selectedIndex + 1)
                selectedIndex = num;
            printf("%d\n", selectedIndex);
            SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
            InvalidateRect(hwnd, NULL, TRUE);
            return 0;
        }
        case VK_RETURN:
        {
            // Обработка клавиши Enter
            // printf("%d\n", selectedIndex);
            if (selectedIndex != -1)
            {
                // Получаем индекс выбранного элемента
                int index = SendMessage(hwndListBox, LB_GETCURSEL, 0, 0);
                // Делаем что-то с выбранным элементом (например, выводим индекс)
                printf("%d\n", index);
                // MessageBox(hwndListBox, to_string(index).c_str(), "Выбранный индекс", MB_OK | MB_ICONINFORMATION);
                InvalidateRect(hwnd, NULL, TRUE);
            }
            return 0;
        }
        }
        break;
    }

    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
}


LRESULT CALLBACK ListWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_CREATE:
    {

        printf("FF");
        hwndListBox = CreateWindowEx(
            0,
            "LISTBOX",
            "list",
            WS_CHILD | WS_VISIBLE | WS_VSCROLL | LBS_NOTIFY,
            10, 10,
            300, 500,
            hwnd,
            NULL,
            hInst,
            NULL);
        // SetFocus(hwndListBox);
        // char str[] = "sdfsfd";
        // SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)str);
        // SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)str);
        // SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)str);
        // SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)str);
        // InvalidateRect(hwnd, NULL, TRUE);
        // InvalidateRect(hwnd, NULL, FALSE);
        return 0;
    }

    case WM_KEYDOWN:
    {
        switch (wParam)
        {
        case VK_UP:
        {
            // Обработка стрелки вверх
            if ((selectedIndex - 1) < 0)
                selectedIndex = 0;
            printf("%d\n", selectedIndex);
            SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
            return 0;
        }

        case VK_DOWN:
        {
            // Обработка стрелки вниз
            int num = SendMessage(hwndListBox, LB_GETCOUNT, 0, 0) - 1;
            if (num < selectedIndex + 1)
                selectedIndex = num;
            printf("%d\n", selectedIndex);
            SendMessage(hwndListBox, LB_SETCURSEL, selectedIndex, 0);
            return 0;
        }
        case VK_RETURN:
        {
            // Обработка клавиши Enter
            printf("%d\n", selectedIndex);
            if (selectedIndex != -1)
            {
                // Получаем индекс выбранного элемента
                int index = SendMessage(hwndListBox, LB_GETCURSEL, 0, 0);
                // Делаем что-то с выбранным элементом (например, выводим индекс)
                printf("%d\n", index);
                // MessageBox(hwndListBox, to_string(index).c_str(), "Выбранный индекс", MB_OK | MB_ICONINFORMATION);
            }
            return 0;
        }
        }
        break;
    }
    // case WM_APP:
    // {
    //     // char str[] = "123";
    //     // SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)str);
    //     // SendMessage(hwndListBox, LB_ADDSTRING, 0, (LPARAM)str);
    //     // InvalidateRect(hwnd, NULL, TRUE);
    // }
    // case WM_PAINT:
    // {
    //     PAINTSTRUCT ps;
    //     HDC hdc = BeginPaint(hwnd, &ps);
    //     EndPaint(hwnd, &ps);
    //     return 0;
    // }
    case WM_COMMAND:
    {
        // Обработка сообщений от элементов управления
        switch (wParam)
        {
        case CBN_SELCHANGE:
        {
            // Получаем выбранный индекс
            // HWND hwndListBox = (HWND)lParam;
            int selectedIndex = SendMessage(hwndListBox, CB_GETCURSEL, 0, 0);
            printf("%d\n", selectedIndex);

            // Получаем текст выбранного элемента
            // char buffer[256];
            // SendMessage(hwndListBox, CB_GETLBTEXT, selectedIndex, (LPARAM)buffer);

            // Выводим информацию о выборе
            break;
        }

        default:
            break;
        }

        break;
    }
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
}
