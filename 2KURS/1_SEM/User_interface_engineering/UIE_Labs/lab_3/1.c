#include <windows.h>
#include <stdio.h>

#define hButtonID 1001
#define hEditID 1002

BOOL RegClass(WNDPROC, LPCTSTR, UINT);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK ChldWndProc(HWND, UINT, WPARAM, LPARAM);

HINSTANCE hInst;
char szClassName[] = "WindowsAppClass";
char szChldClassName[] = "MDIChildClass";
HWND hEdit = NULL, hButton, hWinMDI;

int winWidth = 180, winHeight = 200;
void showError(const char *errorText)
{
    DWORD errorCode = GetLastError();
    LPVOID errorMsg;
    char message[256];

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        errorCode,
        0,
        (LPSTR)&errorMsg,
        0,
        NULL);

    sprintf(message, "Error code: %d\n%s\n%s", errorCode, errorText, (LPSTR)errorMsg);
    MessageBox(NULL, message, "ERROR", MB_ICONERROR);
    LocalFree(errorMsg);
}

BOOL RegClass(WNDPROC Proc, LPCTSTR szName, UINT brBackground)
{
    WNDCLASS wc;
    wc.style = wc.cbClsExtra = wc.cbWndExtra = 0;
    wc.lpfnWndProc = Proc;
    wc.hInstance = hInst;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(brBackground + 1);
    wc.lpszMenuName = (LPCTSTR)NULL;
    wc.lpszClassName = szName;

    return (RegisterClass(&wc) != 0);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    MSG msg;
    HWND hwnd;
    hInst = hInstance;

    if (!RegClass(WndProc, szClassName, COLOR_WINDOW))
    {
        return FALSE;
    }

    hwnd = CreateWindow(
        szClassName,
        "Parent Application",
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        100, 50,
        500, 500,
        0,
        0,
        hInstance,
        NULL);

    if (!hwnd)
    {
        return FALSE;
    }

    while (GetMessage(&msg, 0, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    static char chBuff[80] = "Document";
    printf(chBuff);

    switch (msg)
    {
        // case WM_SIZE:{
        //     winWidth = LOWORD(lParam) / 2;
        //     winHeight = HIWORD(lParam) / 2;
        //     return 0;
        // }
    case WM_CREATE:
    {

        RegClass(ChldWndProc, szChldClassName, COLOR_WINDOW);
       
        hButton = CreateWindow(
            "button",
            "Press me!",
            WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
            130, 85,          // coords M(x, y)
            100, 50,          // width, height
            hwnd,             // parent
            (HMENU)hButtonID, // menu
            hInst,
            NULL);
        return 0;
    }
    case WM_COMMAND:
    {
        switch (LOWORD(wParam))
        {
        // case hEditID:
        // {

        //     if ((HIWORD(wParam)) == EN_ERRSPACE)
        //         MessageBox(hwnd, "No memory", "hEdit", MB_OK);
        //     return 0;
        // }
        case hButtonID: // hButton pressedh
        {
            if (hEdit)
            {
                // printf("dfdsfsdf");
                SendMessage(hEdit, EM_GETLINE, 0, (LPARAM)chBuff);
                // MessageBox(hwnd, chBuff, "chBuff", MB_OK);
            }
            // CLIENTCREATESTRUCT ccs;
            // ccs.hWindowMenu = GetSubMenu(GetMenu(hwnd), 2);
            // ccs.idFirstChild = 50000;

            hWinMDI = CreateWindow(
                szChldClassName,
                "MDI",
                WS_OVERLAPPEDWINDOW | WS_CHILD | WS_VISIBLE,
                100, 50,
                300, 300,
                hwnd,
                0,
                hInst,
                NULL);
             hEdit = CreateWindow(
                "edit",
                NULL,
                WS_VISIBLE | WS_CHILD | WS_BORDER | ES_LEFT,
                30, 40,         // coords M(x, y)
                300, 30,        // width, height
                hWinMDI,        // parent
                (HMENU)hEditID, // menu
                hInst,
                NULL);
            SetWindowText(hEdit, chBuff);
            SetFocus(hEdit);

            return 0;
        }
        default:
            return 0;
        }
    case WM_DESTROY:
    {
        PostQuitMessage(0);
        return 0;
    }
    default:
        return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    }
}
LRESULT CALLBACK ChldWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
    case WM_CREATE:
    {

        return 0;
    }
    case WM_COMMAND:
    {
        switch (LOWORD(wParam))
        {
        case hEditID:
        {

            if ((HIWORD(wParam)) == EN_ERRSPACE)
                MessageBox(hwnd, "No memory", "hEdit", MB_OK);
            return 0;
        }

        default:
            return 0;
        }
    case WM_DESTROY:
    {
        DestroyWindow(hWinMDI);
        return 0;
    }
    default:
        return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    }
}
