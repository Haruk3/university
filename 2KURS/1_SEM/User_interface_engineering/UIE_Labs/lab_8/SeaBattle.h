#pragma once
#include <Windows.h>
#include "commctrl.h"
#include <windowsx.h>
#include <stdio.h>
#include <vector>
#include <random>
#include <ctime>

#define ONE_DECK 1
#define TWO_DECK 2
#define THREE_DECK 3
#define FOUR_DECK 4
#define NEW_GAME 5
#define START_GAME 6
#define RESTART 7
#define ERR 64

BOOL RegClass(HBRUSH BgColor, HCURSOR Cursor, HINSTANCE hInstance, HICON Icon, LPCWSTR Name, WNDPROC Procedure);
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
void AddTable(HWND hWnd);
void Add_Two_Table(HWND hWnd);
void EnableButton();
void DisableButton(); 
void Ship_Building(HWND hWnd, LPARAM lParam);
void End_Ship(HWND hWnd, LPARAM lParam, int s);
int Fill_Polygon_Player(HWND hWnd, LPARAM lParam, COLORREF color);
void Fill_Polygon_Bot();
bool Check_Region_Polygon(int r);
bool Check_Region_Ship(int r);
bool Check_Region(int r);
bool Check_Region_Polygon_Bot(int r);
bool Check_Bot(int ran, int r, int size);
void Move_Bot(HDC hdc, HWND hWnd, int pos);
int Repite(HDC hdc, HWND hWnd, int pos);

enum class Mode { None, One, Two, Three, Four };

struct polygon {
private:
	RECT rect;
	bool ship, injured;
public:
	polygon(RECT rect1) {
		rect = rect1;
		ship = false;
		injured = false;
	}
	bool getShip() { return ship; }
	void setShip(bool ship1) { ship = ship1; }
	bool getInjured() { return injured; }
	void setInjured(bool injured1) { injured = injured1; }
	RECT getRect() { return rect; }
	void Fill(HDC hdc, COLORREF color) { FillRect(hdc, &rect, (HBRUSH)CreateSolidBrush(color)); }
};

struct Ship {
private:
	std::vector<polygon> polygons;
	int size, siz;
public:
	Ship(std::vector<polygon> polygons1, int size1) {
		polygons = polygons1;
		size = siz = size1;
	}
	int get_size() { return size; }
	int get_siz() { return siz; }
	void setSize(int size1) { size = size1; }
	int size_polygons() { return polygons.size(); }
	std::vector<polygon> get_polygon() { return polygons; }
};


