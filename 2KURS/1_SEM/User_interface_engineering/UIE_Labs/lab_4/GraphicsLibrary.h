// GraphicsLibrary.h

#ifndef GRAPHICSLIBRARY_H
#define GRAPHICSLIBRARY_H

#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif

__declspec(dllimport) void DrawEllipse(HDC hdc, int x, int y, int width, int height, COLORREF color);
__declspec(dllimport) void DrawLine(HDC hdc, int x1, int y1, int x2, int y2, COLORREF color);
__declspec(dllimport) void DrawRectangle(HDC hdc, int x, int y, int width, int height, COLORREF color);

#ifdef __cplusplus
}
#endif

#endif // GRAPHICSLIBRARY_H
