use журнал;

delimiter //
-- Староста должен быть членом группы
create trigger if not exists смена_старосты 
before update
on группа for each row
begin
	if old.id_группа != (select группа from студент where номер_студенческого_билета = new.староста) 
	then
		signal sqlstate '45000'
			set message_text = 'Староста должен быть членом группы!';
	end if;
end 

//
-- Год рождения студента не может быть больше года поступления в университет
create trigger if not exists `год_поступления_>_год_рождения`
before insert 
on студент for each row
begin 
	if new.год_поступления <= new.год_рождения
	then
		signal sqlstate '45000' 
			set message_text = 'Год рождения студента не может быть больше года поступления в университет!';
	end if;
end

//
-- Количество часов, выделеных на занятие, должно быть кратно двум
create trigger if not exists сколько_часов
before insert
on дисциплина_в_плане for each row
begin
	if mod(new.часы, 2) != 0
	then
		signal sqlstate '45000' 
			set message_text = 'Количество часов, выделеных на занятие, должно быть кратно двум!';
	end if;
end

//
-- Дата проведения занятия не может быть раньше, чем дата создания учебного плана
create trigger if not exists дата_проведения_занятия
before insert
on занятие for each row
begin
	if year(new.дата) < new.год
	then
		signal sqlstate '45000' 
			set message_text = 'Дата проведения занятия не может быть раньше, чем дата создания учебного плана!';
	end if;
end

//
-- quest_from_den_victor
-- При добалвении занятия у преподавателя нужно учесть
-- что учебный план группы и добавляемого занятия совпадают
create trigger if not exists занятие_у_группы
before insert
on занятие_у_преподавателя for each row
begin 
	set @god = (select год from группа where new.группа = id_группа);
	set @facultet = (select факультет from группа where new.группа = id_группа);
	set @specialnost = (select специальность from группа where new.группа = id_группа);

	if (new.год != @god or new.факультет != @facultet or new.специальность != @specialnost)
	then 
		signal sqlstate '45000' 
			set message_text = 'Занятие не соответствует учебному плану!';
	end if;
end
//


delimiter ;
