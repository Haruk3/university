use журнал;

alter table факультет
add column вышележащее_подразделение tinyint,
add foreign key (вышележащее_подразделение) references факультет(id_факультет);

create table if not exists должность (
	id_должность smallint not null auto_increment,
    название varchar(255) not null,
    оплата float not null,
    primary key (id_должность)
);

create table if not exists должность_у_сотрудника (
	подразделение tinyint not null,
    сотрудник smallint not null,
    должность smallint not null,
    ставка float not null,
    primary key (подразделение, сотрудник, должность),
    foreign key (подразделение) references факультет(id_факультет),
    foreign key (сотрудник) references преподаватель(id_преподаватель),
    foreign key (должность) references должность(id_должность)
);
