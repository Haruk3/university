package org.lab3;
/*
Разработать приложение ftp-клиента, реализующие следующие функции:

Создание и удаление папок;
перемещение по папкам;
чтение списка содержимого папки;
получение, отправка, удаление и переименование файлов.

Для получения списка содержимого папки необходимо использовать активный режим.
Для получения и отправки файлов – пассивный. Приложение использует для обмена с сервером транспортный протокол TCP/IP.
Процесс обмена командами и ответами должен отображаться на экране.*/


import java.io.*;
import java.net.*;

public class FTP {
    public static String getAddrForPassiveMode(String response) {
        String[] strs = response.split("\\(")[1].split("\\)")[0].split(",");
        return strs[0] + "." + strs[1] + "." + strs[2] + "." + strs[3];
    }

    public static int getPortForPassiveMode(String response) {
        String[] parts = response.split(",");
        int port = Integer.parseInt(parts[4].trim()) * 256 + Integer.parseInt(parts[5].replace(")", "").trim());
        return port;
    }

    public static Socket setPassiveMode(PrintWriter writer, BufferedReader reader) throws IOException {
        writer.println("PASV");
        String response = reader.readLine();
        if (!response.startsWith("227")) {
            throw new IOException("ERROR PASV: " + response);
        }
        int port = getPortForPassiveMode(response);
        String addr = getAddrForPassiveMode(response);
        return new Socket(addr, port);
    }

    public static void main(String[] args) {

        String host = "localhost";
        int port = 2121;

        try (Socket socket = new Socket(host, port);
             BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
             BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in))) {

            System.out.println(reader.readLine());
            System.out.print("Username: ");
            String username = userInput.readLine();
            writer.println("USER " + username);

            System.out.println(reader.readLine());

            System.out.print("Password: ");
            String password = userInput.readLine();
            writer.println("PASS " + password);

            String resp = reader.readLine();
            System.out.println(resp);
            if (!resp.startsWith("230")) return;
            writer.println("TYPE I");
            System.out.println(reader.readLine());

            while (true) {
                System.out.println("""
                        cd - Choose directory
                        tree - View directory
                        mkdir - Make dir
                        rmdir - Remove dir
                        download. Download file
                        upload. Upload file
                        delf. Delete file
                        renf. Rename file
                        exit. Exit
                        """);
                String choice = userInput.readLine();
                switch (choice) {
                    case "cd" -> {
                        System.out.print("Enter the path: ");
                        String path = userInput.readLine();
                        writer.println("CWD " + path);
                        System.out.println(reader.readLine());
                    }
                    case "tree" -> {
                        ServerSocket serverSocket = new ServerSocket(2671);
                        writer.println("PORT 127,0,0,1,10,111");
                        System.out.println(reader.readLine());
                        writer.println("LIST");
                        Socket dataSocket = serverSocket.accept();
                        BufferedReader dataReader = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
                        String line;
                        while ((line = dataReader.readLine()) != null) {
                            System.out.println(line);
                        }
                        dataReader.close();
                        System.out.println(reader.readLine());
                        dataSocket.close();
                        System.out.println(reader.readLine());
                        serverSocket.close();
                    }
                    case "mkdir" -> {
                        System.out.print("Введите название директории: ");
                        String dirName = userInput.readLine();
                        writer.println("MKD " + dirName);
                        System.out.println(reader.readLine());
                    }
                    case "rmdir" -> {
                        System.out.print("Введите название директории: ");
                        String remDirName = userInput.readLine();
                        writer.println("RMD " + remDirName);
                        System.out.println(reader.readLine());
                    }
                    case "download" -> {
                        System.out.print("Enter the file to download: ");
                        String remoteFile = userInput.readLine();
                        System.out.print("Save as: ");
                        String destinationPath = userInput.readLine();
                        try (Socket downloadSocket = setPassiveMode(writer, reader);
                             InputStream inputStream = downloadSocket.getInputStream();
                             FileOutputStream fileOutputStream = new FileOutputStream(destinationPath)) {
                            byte[] buffer = new byte[1024];
                            int bytesRead;
                            writer.println("RETR " + remoteFile);
                            System.out.println(reader.readLine());

                            while ((bytesRead = inputStream.read(buffer)) != -1) {
                                fileOutputStream.write(buffer, 0, bytesRead);
                            }
                        }
                        if (reader.readLine().startsWith("226")) System.out.println("SUCCESS");
                        else System.out.println("ERROR");
                    }
                    case "upload" -> {
                        System.out.println("Enter the path to the file: ");
                        String fileToUpload = userInput.readLine();
                        System.out.println("Save as: ");
                        String saveAs = userInput.readLine();
                        try (Socket uploadSocket = setPassiveMode(writer, reader);
                             OutputStream outputStream = uploadSocket.getOutputStream();
                             FileInputStream fileInputStream = new FileInputStream(fileToUpload)) {

                            writer.println("STOR " + saveAs);
                            System.out.println(reader.readLine());

                            byte[] buffer = new byte[4096];
                            int bytesRead;
                            while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                                outputStream.write(buffer, 0, bytesRead);
                                outputStream.flush();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String response = reader.readLine();
                        if (response.startsWith("226")) {
                            System.out.println("SUCCESS");
                        } else {
                            System.out.println("ERROR");
                        }
                    }
                    case "delf" -> {
                        System.out.print("Enter the file to delete: ");
                        String fileToDelete = userInput.readLine();
                        writer.println("DELE " + fileToDelete);
                        System.out.println(reader.readLine());
                    }
                    case "renf" -> {
                        System.out.print("Enter the file to rename: ");
                        String file1Rename = userInput.readLine();
                        System.out.print("New file name: ");
                        String file2Rename = userInput.readLine();
                        writer.println("RNFR " + file1Rename);
                        if (reader.readLine().startsWith("350")) writer.println("RNTO " + file2Rename);
                        System.out.println(reader.readLine());
                    }
                    case "exit" -> {
                        writer.println("QUIT");
                        System.out.println(reader.readLine());
                        return;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
